vcl 4.0;

import std;

backend default {
  .host = "api";
  .port = "8080";
  .connect_timeout = 5s;
  .first_byte_timeout = 10s;
  .between_bytes_timeout = 10s;
  # Health check
  .probe = {
    .url = "/";
    .timeout = 5s;
    .interval = 10s;
    .window = 5;
    .threshold = 3;
  }
}

sub vcl_recv {
    if (req.restarts > 0) {
        set req.hash_always_miss = true;
    }
    unset req.http.forwarded;
}

sub vcl_hit {
  if (obj.ttl >= 0s) {
    # A pure unadultcerated hit, deliver it
    return (deliver);
  }

  if (std.healthy(req.backend_hint)) {
      # The backend is healthy
      # Fetch the object from the backend
      return (restart);
    }

  # No fresh object and the backend is not healthy
  if (obj.ttl + obj.grace > 0s) {
    # Deliver graced object
    # Automatically triggers a background fetch
    return (deliver);
  }

  # No valid object to deliver
    # No healthy backend to handle request
    # Return error
  return (synth(503, "API is down"));
}

sub vcl_backend_response {
    set beresp.ttl = 1s;
    set beresp.grace = 1h;
}
